﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace BehaviourTBS
{
    public enum ActorGroup { Ally, Enemy }

    public class Actor : Sprite
    {
        [Export]
        public string KnownAs { get; set; } = "Unit";
        
        [Export]
        public ActorGroup Group { get; set; }

        [Export]
        public int MaxHP { get; set; } = 10;

        [Export]
        public int Move { get; set; } = 3;

        [Export]
        public int Range { get; set; } = 1;

        public bool CanMove { get; set; } = true;
        public bool CanAttack { get; set; } = false;

        public int HP
        {
            get => hp;

            set
            {
                hp = value;

                //adjust the HP bar's size based on HPP (health %)
                GetNode<TextureRect>("hp/value").RectScale = new Vector2(HPP, 1);
            }
        }

        /// <summary>
        /// Percentage of HP.
        /// </summary>
        public float HPP => (float)HP / MaxHP;

        int hp;

        Stage stage;

        public Vector2i Cell => stage.WorldToMap(GlobalPosition);

        public override void _Ready()
        {
            stage = GetParentOrNull<Stage>();
            HP = MaxHP;
        }

        /// <summary>
        /// Get the panels this actor can move to, from its current location.
        /// </summary>
        public List<Vector2i> GetMovableCells(BattleDirector director)
        {
            var movable = new List<Vector2i>();

            var enemies = director.Actors.Where(a => a.Group != Group);

            //forbid the panels with enemies so it can't move through them
            foreach (var e in enemies)
            {
                stage.Map.Forbid(e.Cell);
            }

            //get the potential movable panels by iterating through surrounding cells that exist, are close enough and are unoccupied
            foreach (var c in Stage.GetSurroundingCells(Cell, Move))
            {
                if (stage.GetCell(c) >= 0 && director.ActorRadar[c] == null)
                {
                    //now trace a path to the panel and check if it's actually reachable and if avoiding obstacles, the length is still shorter than Move
                    var path = stage.Map.FindPath(Cell, c);

                    if (path.Length > 0 && path.Length <= Move + 1)
                    {
                        movable.Add(c);
                    }
                }
            }
            
            //go back to regular state (everything free)
            foreach (var e in enemies)
            {
                stage.Map.Free(e.Cell);
            }

            return movable;
        }

        /// <summary>
        /// Get the panels this actor can attack, from its current location.
        /// </summary>
        public List<Vector2i> GetTargettableCells(BattleDirector director)
        {
            var targettable = new List<Vector2i>();

            //get the potential movable panels by iterating through surrounding cells that exist, are close enough and are unoccupied
            foreach (var c in Stage.GetSurroundingCells(Cell, Move))
            {
                //if the cell exists and the distance between self and this cell is lower than my range, I can attack it
                if (stage.GetCell(c) >= 0 && Vector2i.Manhattan(Cell, c) <= Range)
                {
                    targettable.Add(c);
                }
            }

            return targettable;
        }

        /// <summary>
        /// Find a path to a place. If Director is not null, it will handle blocking cells of enemy units (so you can't go through them); otherwise, the unit will move freely. 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="director"></param>
        public void MoveTo(Vector2i v, BattleDirector director)
        {
            var enemies = director.Actors.Where(a => a.Group != Group);

            //forbid the panels with enemies so it can't move through them
            foreach (var e in enemies)
            {
                stage.Map.Forbid(e.Cell);
            }

            var pth = stage.Map.FindPath(Cell, v);

            foreach (var p in pth)
            {
                path.Add(stage.MapToWorld(p) + stage.CellSize / 2);
            }

            //go back to regular state (everything free)
            foreach (var e in enemies)
            {
                stage.Map.Free(e.Cell);
            }
        }

        public void MoveToFreely(Vector2i v)
        {
            var pth = stage.Map.FindPath(Cell, v);

            foreach (var p in pth)
            {
                path.Add(stage.MapToWorld(p) + stage.CellSize / 2);
            }
        }

        public override void _Process(float dt)
        {
            if (path.Count > 0)
            {
                var motion = path[0] - GlobalPosition;
                var speed = dt * 45f;

                GlobalPosition += motion.Normalized() * speed;

                if (motion.Length() <= speed)
                {
                    path.RemoveAt(0);

                    if (path.Count == 0)
                    {
                        //just to make sure it's snapped to the grid
                        stage.SendToCell(this, Cell);

                        HasArrived?.Invoke();
                    }
                }
            }
        }

        public event Action HasArrived;

        List<Vector2> path = new List<Vector2>();
    }
}
