﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;
using BehaviourTBS.Battle;

namespace BehaviourTBS
{
    /// <summary>
    /// This node controls the battle and its states.
    /// </summary>
    public class BattleDirector : CanvasLayer
    {
        public Control HUD => GetNodeOrNull<Control>("hud");

        BattleState stateHandler;
        BattleState nextStateHandler;

        public Stage Stage { get; private set; }

        /// <summary>
        /// This will set nextStateHandler, so next frame it starts handling the Battle. Also calls current stateHandler (if there's one) Cleanup.
        /// </summary>
        void ChangeState(Battle.BattleState nextStateHandler)
        {
            stateHandler?.OnCleanup();
            stateHandler = null;

            this.nextStateHandler = nextStateHandler;

        }

        public BattleState NextState
        {
            get => nextStateHandler;

            set
            {
                ChangeState(value);
            }
        }

        public override void _Process(float delta)
        {
            if (nextStateHandler != null)
            {
                //set the new state handler, and initialize it
                stateHandler = nextStateHandler;
                stateHandler.OnInit();
                
                HUD.GetNode<Label>("stateInfo/value").Text = $"{ stateHandler.GetType().Name } ";

                //nullify so it doesn't change states every frame
                nextStateHandler = null;
            }
        }

        public override void _UnhandledInput(InputEvent e)
        {
            //if there's a state handler, let it handle input for the Director
            stateHandler?.OnInput(e);
        }

        public void UpdateActorInfo(Actor actor)
        {
            HUD.GetNode<Label>("actorInfo/name").Text = actor.KnownAs;
            HUD.GetNode<Label>("actorInfo/hp/value").Text = $"{ actor.HP }/{ actor.MaxHP }";
        }

        /// <summary>
        /// Show ActorActions if there's an Actor, and connect buttons' events to callbacks. If 'actor' is set to null, disconnect passed callbacks.
        /// </summary>
        public void UpdateActorActions(Actor actor, Action callbackAI, Action callbackMove, Action callbackAttack, Action callbackWait)
        {
            if (actor == null)
            {
                ActorActions.Hide();

                ActorActions.GetNode<SharpButton>("box/ai").HasBeenClicked -= callbackAI;
                ActorActions.GetNode<SharpButton>("box/move").HasBeenClicked -= callbackMove;
                ActorActions.GetNode<SharpButton>("box/attack").HasBeenClicked -= callbackAttack;
                ActorActions.GetNode<SharpButton>("box/wait").HasBeenClicked -= callbackWait;

                return;
            }

            ActorActions.Show();

            var buttonAI = ActorActions.GetNode<SharpButton>("box/ai");
            var buttonMove = ActorActions.GetNode<SharpButton>("box/move");
            var buttonAttack = ActorActions.GetNode<SharpButton>("box/attack");
            var buttonWait = ActorActions.GetNode<SharpButton>("box/wait");
            
            buttonAI.Disabled = true;// !actor.CanMove;
            buttonMove.Disabled = !actor.CanMove || actor.GetMovableCells(this).Count == 0;
            buttonAttack.Disabled = !actor.CanAttack;

            buttonAI.HasBeenClicked += callbackAI;
            buttonMove.HasBeenClicked += callbackMove;
            buttonAttack.HasBeenClicked += callbackAttack;
            buttonWait.HasBeenClicked += callbackWait;
        }

        public Control ActorActions => HUD.GetNode<Control>("actorActions");

        /// <summary>
        /// This grid will store the actor's IDs that are attached to a tile, so you can track them later (whenever an actor moves to a [X, Y] location, this position in the grid will store its ID.
        /// </summary>
        public Grid2<Actor> ActorRadar { get; private set; }

        public List<Actor> Actors { get; set; } = new List<Actor>();

        public Sprite Cursor { get; private set; }

        public override void _Ready()
        {
            Stage = GetNodeOrNull<Stage>("../stage");
            ActorRadar = new Grid2<Actor>(Stage.Rect);

            //send the cursor to the stage
            Cursor = GetNode<Sprite>("cursor");
            RemoveChild(Cursor);
            Stage.AddChild(Cursor);

            //find all actors, check if they're inside the map's boundaries and in an available position; if so, store their IDs in their cells on the grid
            //who is not on the stage will be ignored by the director as they won't be clickable and won't be added to the list of actors
            foreach (var actor in Stage.GetChildren().OfType<Actor>())
            {
                //get grid position
                var pos = Stage.WorldToMap(actor.GlobalPosition);

                //check if its position is within the stage's rect (bounds) 
                if (Stage.Rect.Contains(pos))
                {
                    ActorRadar[pos] = actor;
                    Actors.Add(actor);
                }
            }

            ChangeState(new BSNavigate(this));
            
            GD.Print($"<BattleDirector> Initialised. Units: {Actors.Count} ({ Actors.Where(a => a.Group == ActorGroup.Ally).Count() } allies and { Actors.Where(a => a.Group == ActorGroup.Enemy).Count() } enemies).");
        }
    }
}
