﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BehaviourTBS.Battle
{
    public class BSAwaitMove : BattleState
    {
        Actor actor;
        Vector2i goalPos;
        
        public BSAwaitMove(BattleDirector director, Actor actor, Vector2i goalPos) : base(director)
        {
            Director = director;
            this.actor = actor;
            this.goalPos = goalPos;
        }

        public override void OnInit()
        {
            //un-register this actor from its current cell and register it to its goal
            Director.ActorRadar[actor.Cell] = null;
            Director.ActorRadar[goalPos] = actor;

            //order unit to move to the place, and track its arrival
            actor.MoveTo(goalPos, Director);
            actor.HasArrived += OnActorHasArrived;

            actor.CanMove = false;
        }

        public override void OnCleanup()
        {
            actor.HasArrived -= OnActorHasArrived;
        }

        void OnActorHasArrived()
        {
            //go back to menu
            Director.NextState = new BSMenu(Director, actor);
        }
    }
}
