﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace BehaviourTBS.Battle
{
    public class BSMenu : BattleState
    {
        Actor actor;

        public BSMenu(BattleDirector director, Actor actor) : base(director)
        {
            Director = director;

            this.actor = actor;
        }

        public override void OnInit()
        {
            Cursor.Show();
            Stage.SendToCell(Cursor, actor.Cell);

            Director.UpdateActorActions(actor, OnHasClickedAI, OnHasClickedMove, OnHasClickedAttack, OnHasClickedWait);
        }

        public override void OnInput(InputEvent e)
        {
            if (e is InputEventMouseButton m)
            {
                if (m.IsPressed() && m.ButtonIndex == (int)ButtonList.Right)
                {
                    Director.NextState = new BSNavigate(Director);
                }
            }
        }

        public override void OnCleanup()
        {
            Director.UpdateActorActions(null, OnHasClickedAI, OnHasClickedMove, OnHasClickedAttack, OnHasClickedWait);
        }

        void OnHasClickedAI()
        {

        }

        void OnHasClickedMove()
        {
            Director.NextState = new BSMove(Director, actor);
        }

        void OnHasClickedAttack()
        {

        }

        void OnHasClickedWait()
        {
            Director.NextState = new BSNavigate(Director);
        }
    }
}
