﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;

namespace BehaviourTBS.Battle
{
    public class BSMove : BattleState
    {
        Actor actor;
        List<Node2D> markers = new List<Node2D>();
        List<Vector2i> movable;

        public BSMove(BattleDirector director, Actor actor) : base(director)
        {
            Director = director;
            this.actor = actor;
        }

        public override void OnInit()
        {
            //create markers on the floor for each movable cell, showing where this actor can move to
            var markerPS = GD.Load<PackedScene>("res://src/entity/marker.tscn");
            
            movable = actor.GetMovableCells(Director);

            foreach (var m in movable)
            {
                var marker = markerPS.Instance() as Sprite;

                //paint it blue
                marker.Modulate = new Color(0.25f, 0.25f, .8f);

                Stage.AddChild(marker);
                Stage.SendToCell(marker, m);

                //store the marker in a list so we can delete them later
                markers.Add(marker);
            }

            //register mouseclick so we can check if you clicked a movable cell
            Stage.HasClicked += OnHasClicked;
        }

        public override void OnInput(InputEvent e)
        {
            if (e is InputEventMouseButton m)
            {
                if (m.IsPressed() && m.ButtonIndex == (int)ButtonList.Right)
                {
                    Director.NextState = new BSMenu(Director, actor);
                }
            }
        }

        public override void OnCleanup()
        {
            //un-register events -- NEVER FORGET!
            Stage.HasClicked -= OnHasClicked;

            //free the markers
            foreach (var m in markers)
            {
                m.QueueFree();
            }
        }

        void OnHasClicked(Vector2i v)
        {
            //if 'movable' contains 'v', it means I can move there
            if (movable.Contains(v))
            {
                Cursor.Hide();
                Director.NextState = new BSAwaitMove(Director, actor, v);
            }
        }
    }
}
