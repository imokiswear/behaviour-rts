﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Godot;

namespace BehaviourTBS.Battle
{
    public class BSNavigate : BattleState
    {
        public BSNavigate(BattleDirector director) : base(director)
        {
            Director = director;
        }

        public override void OnInit()
        {
            Stage.HasHovered += OnStageHasHovered;
            Stage.HasClicked += OnStageHasClicked;
        }

        public override void OnCleanup()
        {
            //unregister events -- never forget
            Stage.HasHovered -= OnStageHasHovered;
            Stage.HasClicked -= OnStageHasClicked;
        }

        void OnStageHasHovered(Vector2i v)
        {
            //if hovering a stage cell, check if there's somebody there and print it
            if (Stage.GetCell(v) >= 0)
            {
                var hovered = Director.ActorRadar[v];

                //GD.Print($"<BattleDirector/BSNavigate> Hovered { v }. { hovered?.KnownAs ?? "Nobody" } is there.");

                if (hovered != null)
                {
                    Director.UpdateActorInfo(hovered);

                    Stage.SendToCell(Cursor, v);
                }

                //if there's no actor at the hovered cell, hide the actor info panel
                HUD.GetNode<Control>("actorInfo").Visible = hovered != null;

                Cursor.Visible = hovered != null;
            }
        }

        void OnStageHasClicked(Vector2i v)
        {
            if (Stage.GetCell(v) >= 0)
            {
                var hovered = Director.ActorRadar[v];

                GD.Print($"<BattleDirector/BSNavigate> Clicked { v }. { hovered?.KnownAs ?? "Nobody" } is there.");

                //if clicked a cell and there's someone there, enter Menu state
                if (hovered != null)
                {
                    Director.NextState = new BSMenu(Director, hovered);
                }
            }
        }
    }
}
