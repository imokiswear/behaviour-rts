﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;

namespace BehaviourTBS.Battle
{
    public abstract class BattleState
    {
        public BattleDirector Director { get; protected set; }

        public Stage Stage => Director.Stage;
        public Control HUD => Director.HUD;
        public Sprite Cursor => Director.Cursor;

        

        public BattleState(BattleDirector director)
        {
            Director = director;
        }

        public virtual void OnInit()
        {

        }

        public virtual void OnInput(Godot.InputEvent e)
        {

        }
        
        public virtual void OnCleanup()
        {

        }
    }
}
