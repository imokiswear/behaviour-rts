﻿using Godot;
using System.Collections.Generic;
using System;

namespace BehaviourTBS
{
    /// <summary>
    /// Helper class for AStar points grid.
    /// </summary>
    public class AStarMap
    {
        public Rect2i Rect { get; }

        AStar2D astar = new AStar2D();
        public Grid2<bool> IsFree { get; private set; }

        Stage _stage;

        /// <summary>
        /// A <see cref="AStarMap"/> 
        /// </summary>
        /// <param name="stage">The reference tilemap to create the A* map.</param>
        public AStarMap(Stage stage)
        {
            var usedRect = stage.GetUsedRect();

            //create a rect using the tilemap's position and size
            Rect = new Rect2i(usedRect.Position, usedRect.Size);
            IsFree = new Grid2<bool>(Rect);

            _stage = stage;
            //make a point for each cell in the tilemap, and connect them
            MakePoints(stage);
            ConnectPoints();


            //forbid the A* map cells based on unused cells of the world's tilemap
            foreach (var p in Rect.Points)
            {
                if (stage.GetCell(p.X, p.Y) < 0)
                {
                    Forbid(p);
                }
            }

            //Game.Log(GameLog.Stage, $"Stage's A*Map created. Map size: { Rect.Width }x{ Rect.Height }.");
        }

        /// <summary>
        /// Initialize the points of the A* map by creating them and assigning to the corresponding indexes.
        /// </summary>
        void MakePoints(Stage stage)
        {
            foreach (var point in Rect.Points)
            {
                astar.AddPoint(MakeID(point), new Vector2(point.X, point.Y));
                IsFree[point] = stage.GetCell(point.X, point.Y) >= 0;
            }
        }

        public void PassHeightmap(Grid2<float> heightmap)
        {
            foreach (var p in heightmap.Points)
            {
                astar.SetPointPosition(MakeID(p), new Vector2(p.X, p.Y));
            }
        }

        /// <summary>
        /// Returns whether the tile is a valid walkable place or not.
        /// </summary>
        public bool IsWalkable(Vector2i p)
        {
            return (Rect.Contains(p) && IsFree[p]);
        }

        /// <summary>
        /// Connect the points of the A* map with their neighbours.
        /// </summary>
        void ConnectPoints()
        {
            foreach (var p in Rect.Points)
            {
                if (IsFree[p])
                {
                    Free(p);
                }
            }
        }

        /// <summary>
        /// Frees the node at the given position.
        /// </summary>
        public void Free(Vector2i p) => Free(p.X, p.Y);

        /// <summary>
        /// Frees the node at the given position.
        /// </summary>
        public void Free(int x, int y)
        {
            var thisOne = new Vector2i(x, y);

            IsFree[thisOne] = true;

            var L = Mathf.Max(Rect.From.X, x - 1);
            var U = Mathf.Max(Rect.From.Y, y - 1);
            var R = Mathf.Min(Rect.To.X, x + 1);
            var D = Mathf.Min(Rect.To.Y, y + 1);
            var thisID = MakeID(thisOne);

            foreach (var p in new Rect2i(L, U, R - L + 1, D - U + 1).Points)
            {
                var neighbourID = MakeID(p.X, p.Y);

                if (IsFree[p] && Vector2i.Manhattan(thisOne, p) == 1)
                {
                    //don't reconnect the points if they're listed as cutOnes
                    if (CutOnes.Contains(new Tuple<Vector2i, Vector2i>(thisOne, p)))
                    {
                        continue;
                    }

                    if (!astar.ArePointsConnected(thisID, neighbourID))
                    {
                        astar.ConnectPoints(thisID, neighbourID, true);
                    }
                }
            }
        }

        /// <summary>
        /// Forbids the node at the given position.
        /// </summary>
        public void Forbid(Vector2i p) => Forbid(p.X, p.Y);

        List<Tuple<Vector2i, Vector2i>> CutOnes = new List<Tuple<Vector2i, Vector2i>>();

        /// <summary>
        /// Cut bridge from point A to point B (but not from B to A)!
        /// </summary>
        public void Cut(Vector2i a, Vector2i b)
        {
            var aID = MakeID(a);
            var bID = MakeID(b);

            if (astar.ArePointsConnected(aID, bID))
            {
                astar.DisconnectPoints(aID, bID);
            }
        }

        public void CutAndTrack(Vector2i a, Vector2i b)
        {
            CutOnes.Add(new Tuple<Vector2i, Vector2i>(a, b));
            Cut(a, b);
        }

        public void ReconnectCutOnes()
        {
            foreach (var a in CutOnes)
            {
                Connect(a.Item1, a.Item2);
            }

            CutOnes.Clear();
        }

        /// <summary>
        /// Connect point A to point B (but not B to A)!
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        public void Connect(Vector2i a, Vector2i b)
        {
            var aID = MakeID(a);
            var bID = MakeID(b);

            if (!astar.ArePointsConnected(aID, bID))
            {
                astar.ConnectPoints(aID, bID);
            }
        }

        /// <summary>
        /// Forbids the node at the given position.
        /// </summary>
        public void Forbid(int x, int y)
        {
            var thisOne = new Vector2i(x, y);

            IsFree[thisOne] = false;

            var ID = MakeID(x, y);
            var L = Mathf.Max(Rect.From.X, thisOne.X - 1);
            var U = Mathf.Max(Rect.From.Y, thisOne.Y - 1);
            var R = Mathf.Min(Rect.To.X, thisOne.X + 1);
            var D = Mathf.Min(Rect.To.Y, thisOne.Y + 1);

            foreach (var p in new Rect2i(L, U, R - L + 1, D - U + 1).Points)
            {
                var toID = MakeID(p);

                if (astar.ArePointsConnected(ID, toID) && Vector2i.Manhattan(p, thisOne) == 1)
                {
                    astar.DisconnectPoints(toID, ID);
                }
            }
        }

        /// <summary>
        /// Gets a point path from p1 to p2.
        /// </summary>
        public Vector2[] FindPath(Vector2i p1, Vector2i p2)
        {
            var path = astar.GetPointPath(MakeID(p1), MakeID(p2));

            return path;
        }

        public Vector2[] FindPath2(Vector2i p1, Vector2i p2)
        {
            var rawPath = astar.GetPointPath(MakeID(p1), MakeID(p2));
            var path = new Vector2[rawPath.Length];

            for (var i = 0; i < rawPath.Length; i++)
            {
                path[i] = new Vector2(rawPath[i].x, rawPath[i].y);
            }

            return path;
        }

        //Aliases
        int Flatten(Vector2i p) => Flatten(p.X, p.Y);
        int MakeID(Vector2i p) => MakeID(p.X, p.Y);

        /// <summary>
        /// Converts a 2D coord to 1D.
        /// </summary>
        int Flatten(int x, int y)
        {
            return x + y * Rect.Width;
        }

        /// <summary>
        /// Flattens a 2D coord and convert it to the grid's boundaries.
        /// </summary>
        int MakeID(int x, int y)
        {
            var pos = new Vector2i(x, y) - Rect.From;
            return Flatten(pos);
        }
    }
}
