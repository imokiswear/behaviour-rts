﻿using System;
using System.Collections.Generic;

namespace BehaviourTBS
{
    /// <summary>
    /// Bi-dimensional grid to store values, works like arrays (even with array-like accessors), but accepts negative indexes.
    /// </summary>
    public class Grid2<T>
    {
        /// <summary>
        /// This is where the data is stored.
        /// </summary>
        T[,] data;

        public Rect2i Rect { get; }

        public Grid2(Rect2i rect, T def = default(T))
        {
            Rect = rect;

            data = new T[Rect.Width, Rect.Height];
            Fill(def);
        }

        public bool Contains(Vector2i p)
        {
            return (p.X >= Rect.From.X && p.Y >= Rect.From.Y && p.X <= Rect.To.X && p.Y <= Rect.To.Y);
        }

        /// <summary>
        /// Use this to iterate through the points of the grid (foreach).
        /// </summary>
        public Vector2i[] Points
        {
            get => Rect.Points;
        }

        /// <summary>
        /// Use this to iterate through the data of the grid (foreach).
        /// </summary>

        public void Fill(T value)
        {
            foreach (var p in Rect.Points)
            {
                Set(new Vector2i(p.X, p.Y), value);
            }
        }

        void Set(Vector2i p, T value)
        {
            if (!Contains(p))
            {
                throw new Exception($"The Grid2's index you are trying to modify {p} does NOT exist. Bounds: {Rect}");
            }

            var index = p - Rect.From;
            data[index.X, index.Y] = value;
        }

        T Get(Vector2i p)
        {
            if (!Contains(p))
            {
                throw new Exception($"The Grid2's index you are trying to get data from {p} does NOT exist. Bounds: {Rect}");
            }

            var index = p - Rect.From;
            return data[index.X, index.Y];
        }

        public T this[int x, int y]
        {
            get => Get(new Vector2i(x, y));

            set => Set(new Vector2i(x, y), value);
        }

        public T this[Vector2i p]
        {
            get => Get(p);

            set => Set(p, value);
        }
    }
}
