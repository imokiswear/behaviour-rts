﻿using System.Collections.Generic;

using Godot;

namespace BehaviourTBS
{
    public struct Rect2i
    {
        /// <summary>
        /// The top-left corner of the rectangle.
        /// </summary>
        public Vector2i From { get; }

        /// <summary>
        /// The size of the rectangle.
        /// </summary>
        public Vector2i Size { get; }

        /// <summary>
        /// The bottom-right corner of the rectangle.
        /// </summary>
        public Vector2i To { get => From + Size - Vector2i.One; }

        /// <summary>
        /// The width of the rectangle.
        /// </summary>
        public int Width { get => Size.X; }

        /// <summary>
        /// The height of the rectangle.
        /// </summary>
        public int Height { get => Size.Y; }

        public Vector2i Center => From + Size / 2;

        public Rect2i(Vector2i pos, Vector2i size)
        {
            From = pos;
            Size = size;
        }

        public Rect2i(int x, int y, int width, int height)
        {
            From = new Vector2i(x, y);
            Size = new Vector2i(width, height);
        }
        public Rect2i(Vector2i pos, int width, int height)
        {
            From = pos;
            Size = new Vector2i(width, height);
        }

        public override string ToString()
        {
            return $"Rect2i(From: { From }, Size: { Size })";
        }

        public bool Contains(Vector2i p)
        {
            return (p.X >= From.X && p.Y >= From.Y && p.X <= To.X && p.Y <= To.Y);
        }

        public static explicit operator Rect2i(Rect2 r) => new Rect2i(r.Position, r.Size);

        /// <summary>
        /// The points contained by the rectangle, use this to iterate through its points.
        /// </summary>
        public Vector2i[] Points
        {
            get
            {
                var points = new Vector2i[Width * Height];
                var itr = 0;

                for (var X = From.X; X <= To.X; X++)
                {
                    for (var Y = From.Y; Y <= To.Y; Y++)
                    {
                        points[itr] = new Vector2i(X, Y);
                        itr++;

                        //yield return new Point2(X, Y);
                    }
                }

                return points;
            }
        }
    }
}
