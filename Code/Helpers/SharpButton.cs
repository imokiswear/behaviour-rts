﻿using System;

namespace BehaviourTBS
{
    /// <summary>
    /// This inherits Button to let them emit C# signals (events).
    /// </summary>
    public class SharpButton : Godot.Button
    {
        public event Action HasBeenClicked;

        public override void _Ready()
        {
            Connect("pressed", this, "OnHasBeenClicked");
        }

        void OnHasBeenClicked()
        {
            HasBeenClicked?.Invoke();
        }
    }
}
