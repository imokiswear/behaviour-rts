﻿using Godot;
using System;
using System.Collections.Generic;

namespace BehaviourTBS
{
    public struct Vector2i
    {
        public int X { get; }
        public int Y { get; }

        public Vector2i(int x, int y)
        {
            X = x;
            Y = y;
        }
        public Vector2i(float x, float y)
        {
            X = (int)Math.Floor(x);
            Y = (int)Math.Floor(y);
        }
        public Vector2i(Vector2i p)
        {
            X = p.X;
            Y = p.Y;
        }
        public Vector2i(Vector2 p)
        {
            X = (int)p.x;
            Y = (int)p.y;
        }

        public static Rect2i GetBounds(List<Vector2i> points)
        {
            int? r = null, l = null, u = null, d = null;

            foreach (var p in points)
            {
                if (r == null || p.X > r.Value)
                {
                    r = p.X;
                }
                if (l == null || p.X < l.Value)
                {
                    l = p.X;
                }

                if (d == null || p.Y > d.Value)
                {
                    d = p.Y;
                }
                if (u == null || p.Y < u.Value)
                {
                    u = p.Y;
                }
            }

            return new Rect2i(l.Value, u.Value, r.Value - l.Value, d.Value - u.Value);
        }

        public static int AngleBetween(Vector2i p1, Vector2i p2)
        {
            return (int)(Math.Atan2(p2.Y - p1.Y, p2.X - p1.X) * 180 / Math.PI) + 90;
        }

        public static Vector2i One => new Vector2i(1, 1);
        public static Vector2i UnitX => new Vector2i(1, 0);
        public static Vector2i UnitY => new Vector2i(0, 1);

        public override string ToString()
        {
            return "(" + X + ", " + Y + ")";
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }


        public static implicit operator Vector2i(Vector2 v)
        {
            return new Vector2i(v.x, v.y);
        }

        public Vector2 ToVec2()
        {
            return new Vector2(X, Y);
        }
        public static bool operator ==(Vector2i c1, Vector2i c2)
        {
            return c1.Equals(c2);
        }

        public static bool operator !=(Vector2i c1, Vector2i c2)
        {
            return !c1.Equals(c2);
        }

        public static int Manhattan(Vector2i p1, Vector2i p2)
        {
            var diff = p1 - p2;
            return (int)(Mathf.Abs(diff.X) + Mathf.Abs(diff.Y));
        }

        public static explicit operator Vector2(Vector2i p)
        {
            return new Vector2(p.X, p.Y);
        }
        public static Vector2i operator +(Vector2i p1, Vector2i p2)
        {
            return new Vector2i(p1.X + p2.X, p1.Y + p2.Y);
        }
        public static Vector2i operator -(Vector2i p1, Vector2i p2)
        {
            return new Vector2i(p1.X - p2.X, p1.Y - p2.Y);
        }
        public static Vector2i operator +(Vector2i p1, int v)
        {
            return new Vector2i(p1.X + v, p1.Y + v);
        }
        public static Vector2i operator -(Vector2i p1, int v)
        {
            return new Vector2i(p1.X - v, p1.Y - v);
        }
        public static Vector2i operator -(Vector2i p1)
        {
            return new Vector2i(-p1.X, -p1.Y);
        }
        public static Vector2i operator *(Vector2i p1, int v)
        {
            return new Vector2i(p1.X * v, p1.Y * v);
        }
        public static Vector2i operator /(Vector2i p1, int v)
        {
            return new Vector2i(p1.X / v, p1.Y / v);
        }
    }
}
