using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Godot;

namespace BehaviourTBS
{
	public class Stage : TileMap
	{
		public AStarMap Map { get; private set; }
		public Rect2i Rect { get; private set; }

		public override void _Ready()
		{
			Rect = (Rect2i)GetUsedRect();
			Map = new AStarMap(this);
			
			foreach(var p in Rect.Points)
			{

			}

			GD.Print($"<Stage> Initialised. Size: { Rect.Width }x{ Rect.Height }.");
		}

		Vector2i? hoveredCell;

		/// <summary>
		/// This is used to calculate how many panels would be necessary to add to the pool.
		/// </summary>
		static int MaxNeededPanels(int range)
		{
			return 2 * (range + range * range);
		}

		public int GetCell(Vector2i v)
		{
			return GetCell(v.X, v.Y);
		}

		/// <summary>
		/// Array of panels surrounding a spot.
		/// </summary>
		public static Vector2i[] GetSurroundingCells(Vector2i v, int m)
		{
			int iX = v.X;
			int iY = v.Y - m;
			int iBegin = 0;
			int iSize = 1;
			int i = 0;
			int f = 0;

			var panels = new Vector2i[MaxNeededPanels(m) + 1];

			while (true)
			{
				panels[f] = new Vector2i(iX + i, iY);

				i++;
				f++;

				if (iY == v.Y + m)
				{
					break;
				}

				if (i == iSize)
				{
					iY++;

					var decrease = iY > v.Y;

					iBegin += decrease ? 1 : -1;
					iSize -= decrease ? 2 : -2;

					iX = v.X + iBegin;
					i = 0;
				}

			}

			return panels;
		}
		
		public event Action<Vector2i> HasHovered, HasClicked;

		public void SendToCell(Node2D n, Vector2i v)
		{
			n.GlobalPosition = MapToWorld((Vector2)v) + CellSize / 2f;
		}

		public override void _UnhandledInput(InputEvent e)
		{
			if (e is InputEventMouseMotion)
			{
				var mousePos = WorldToMap(GetGlobalMousePosition());
				
				//if the last hovered cell is different than the current one (or no cell has been hovered yet), emit a signal that it's changed
				if (hoveredCell == null || hoveredCell != mousePos)
				{
					hoveredCell = mousePos;
					HasHovered?.Invoke(mousePos);
				}
			}

			if (e is InputEventMouseButton m)
			{
				if (m.IsPressed() && m.ButtonIndex == (int)ButtonList.Left)
				{
					//check if the hovered cell is not null, cast it to Vector2i (non-nullable) and store in 'v', so I can use it later w/o relying on hoveredCell.Value
					if (hoveredCell is Vector2i v && GetCell(v) >= 0)
					{
						HasClicked?.Invoke(v);
					}
				}
			}
		}
	}
}
